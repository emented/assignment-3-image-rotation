//
// Created by Илья Поветин on 20.12.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_ROTATOR_H
#define IMAGE_TRANSFORMER_IMAGE_ROTATOR_H

#include "structures/image_struct.h"

struct image rotate_image(struct image image);

#endif //IMAGE_TRANSFORMER_IMAGE_ROTATOR_H
