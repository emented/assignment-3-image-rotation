//
// Created by Илья Поветин on 20.12.2022.
//

#ifndef IMAGE_TRANSFORMER_FILE_DESCRIPTOR_WORKER_H
#define IMAGE_TRANSFORMER_FILE_DESCRIPTOR_WORKER_H

#include "stdio.h"

enum descriptor_operation_status {
    DESCRIPTOR_OPERATION_SUCCESS = 0,
    DESCRIPTOR_OPERATION_FILENAME_IS_NULL,
    DESCRIPTOR_OPERATION_FILE_NOT_FOUND,
    DESCRIPTOR_OPERATION_FILE_IS_NULL
};

enum descriptor_operation_status open_file_for_read_and_get_status(FILE **file, const char *file_name);

enum descriptor_operation_status open_file_for_write_and_get_status(FILE **file, const char *file_name);

enum descriptor_operation_status close_file_and_get_status(FILE **file);


#endif //IMAGE_TRANSFORMER_FILE_DESCRIPTOR_WORKER_H
