//
// Created by Илья Поветин on 20.12.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_STRUCT_H
#define IMAGE_TRANSFORMER_IMAGE_STRUCT_H

#include <inttypes.h>

#define PIXEL_SIZE 4

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

#endif //IMAGE_TRANSFORMER_IMAGE_STRUCT_H
