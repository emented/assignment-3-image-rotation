//
// Created by Илья Поветин on 20.12.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_WORKER_H
#define IMAGE_TRANSFORMER_IMAGE_WORKER_H

#include "image_struct.h"

#include "stdio.h"

size_t get_padding_size(struct image const *img);

#endif //IMAGE_TRANSFORMER_IMAGE_WORKER_H
