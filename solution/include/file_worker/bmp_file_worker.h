//
// Created by Илья Поветин on 20.12.2022.
//

#ifndef IMAGE_TRANSFORMER_BMP_FILE_WORKER_H
#define IMAGE_TRANSFORMER_BMP_FILE_WORKER_H

#include "structures/image_struct.h"

#include "stdio.h"

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_ARGUMENTS,
    READ_ALLOCATION_ERROR
};

enum read_status from_bmp(FILE *in, struct image *img);

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_ARGUMENTS,
    WRITE_ERROR
};

enum write_status to_bmp(FILE *out, struct image const *img);


#endif //IMAGE_TRANSFORMER_BMP_FILE_WORKER_H
