#include "file_worker/bmp_file_worker.h"
#include "descriptor/file_descriptor_worker.h"
#include "rotator/image_rotator.h"

#include "stdio.h"
#include "stdlib.h"

#define EXIT_FAILTURE 1
#define EXIT_SUCCESS 0

#define ARGUMENTS_AMOUNT_MISMATCH_MESSAGE "You need to path 3 arguments"
#define SOURCE_FILE_NOT_FOUND_MESSAGE "Source file not found"
#define TARGET_FILE_CANNOT_BE_CREATED_MESSAGE "Target file cannot be created or overwritten"
#define MALLOC_TROUBLE_MESSAGE "Met troubles during malloc"
#define BMP_CONVERSION_ERROR_MESSAGE "Error during converting to BMP structure"

static void print_error(const char* message) {
    fprintf(stderr, "%s", message);
}

int main(int argc, char **argv) {

    if (argc != 3) {
        print_error(ARGUMENTS_AMOUNT_MISMATCH_MESSAGE);
        return EXIT_FAILTURE;
    }

    FILE *source_image;
    FILE *rotated_image;

    if (open_file_for_read_and_get_status(&source_image, argv[1]) != 0) {
        print_error( SOURCE_FILE_NOT_FOUND_MESSAGE);
        return EXIT_FAILTURE;
    }

    if (open_file_for_write_and_get_status(&rotated_image, argv[2]) != 0) {
        print_error(TARGET_FILE_CANNOT_BE_CREATED_MESSAGE);
        close_file_and_get_status(&source_image);
        return EXIT_FAILTURE;
    }

    struct image *image_struct = malloc(sizeof(struct image));

    if (image_struct == NULL) {
        print_error(MALLOC_TROUBLE_MESSAGE);
        close_file_and_get_status(&source_image);
        close_file_and_get_status(&rotated_image);
        return EXIT_FAILTURE;
    }

    enum read_status read_status = from_bmp(source_image, image_struct);

    if (read_status == 0) {

        struct image rotated_struct = rotate_image(*image_struct);

        free(image_struct->data);
        free(image_struct);

        to_bmp(rotated_image, &rotated_struct);

        close_file_and_get_status(&source_image);
        close_file_and_get_status(&rotated_image);
        free(rotated_struct.data);

        return EXIT_SUCCESS;

    }

    close_file_and_get_status(&source_image);
    close_file_and_get_status(&rotated_image);
    free(image_struct->data);
    free(image_struct);
    print_error(BMP_CONVERSION_ERROR_MESSAGE);
    return EXIT_FAILTURE;

}
