//
// Created by Илья Поветин on 20.12.2022.
//
#include "file_worker/bmp_file_worker.h"

#include "structures/bmp_header.h"
#include "structures/image_struct.h"
#include "structures/image_worker.h"

#include "stdio.h"
#include "stdlib.h"

#define DEFAULT_FILE_TYPE 19778
#define DEFAULT_SIZE 40
#define DEFAULT_BIT_COUNT 24
#define DEFAULT_PIXELS_PER_METER 2834

enum read_status from_bmp(FILE *in, struct image *img) {

    if (in == NULL || img == NULL) {
        return READ_INVALID_ARGUMENTS;
    }

    size_t bmp_header_size = sizeof(struct bmp_header);
    size_t pixel_size = sizeof(struct pixel);

    struct bmp_header *new_header = malloc(bmp_header_size);

    if (new_header == NULL) {
        return READ_ALLOCATION_ERROR;
    }

    size_t read_status = fread(new_header, bmp_header_size, 1, in);

    if (read_status != 1) {
        return READ_INVALID_HEADER;
    }

    struct pixel *new_data = malloc(new_header->biWidth * new_header->biHeight * pixel_size);
    if (new_data == NULL) {
        free(new_header);
        return READ_ALLOCATION_ERROR;
    }

    img->width = new_header->biWidth;
    img->height = new_header->biHeight;
    img->data = new_data;

    size_t padding_size = get_padding_size(img);

    fseek(in, new_header->bOffBits, SEEK_SET);
    free(new_header);

    for (uint32_t i = 0; i < img->height; i++) {
        struct pixel* read_pos = img->data + i * img->width;
        read_status = fread(read_pos, pixel_size, img->width, in);
        if (read_status != img->width) {
            return READ_INVALID_BITS;
        }
        fseek(in, (int8_t) padding_size, SEEK_CUR);
    }
    return READ_OK;

}

enum write_status to_bmp(FILE *out, struct image const *img) {

    if (out == NULL || img == NULL) {
        return WRITE_INVALID_ARGUMENTS;
    }

    size_t bmp_header_size = sizeof(struct bmp_header);
    size_t padding_size = get_padding_size(img);
    size_t pixel_size = sizeof(struct pixel);

    struct bmp_header new_header = {
            .bfType = DEFAULT_FILE_TYPE,
            .bfReserved = 0,
            .biCompression = 0,
            .bOffBits = bmp_header_size,
            .biSize = DEFAULT_SIZE,
            .biPlanes = 0,
            .biXPelsPerMeter = DEFAULT_PIXELS_PER_METER,
            .biYPelsPerMeter = DEFAULT_PIXELS_PER_METER,
            .biClrImportant = 0,
            .biClrUsed = 0,
            .biBitCount = DEFAULT_BIT_COUNT,
            .biHeight = img->height,
            .biWidth = img->width,
            .biSizeImage = pixel_size * img->height * (img->width + padding_size),
            .bfileSize = bmp_header_size * (pixel_size * img->height * (img->width + padding_size))
    };

    size_t write_status = fwrite(&new_header, bmp_header_size, 1, out);

    if (write_status != 1) {
        return WRITE_ERROR;
    }

    int padding_zero_byte = 0;

    for (uint32_t i = 0; i < new_header.biHeight; i++) {
        struct pixel* write_pos = img->data + i * new_header.biWidth;
        size_t line_write_status = fwrite(write_pos, pixel_size, new_header.biWidth, out);
        if (line_write_status != new_header.biWidth) {
            return WRITE_ERROR;
        }
        size_t padding_write_status = fwrite(&padding_zero_byte, padding_size, 1, out);
        if (padding_write_status == 0) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;

}


