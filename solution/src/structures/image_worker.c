//
// Created by Илья Поветин on 20.12.2022.
//

#include "structures/image_worker.h"

size_t get_padding_size(struct image const *img) {
    return (PIXEL_SIZE - ((sizeof(struct pixel) * img->width) % PIXEL_SIZE));
}
