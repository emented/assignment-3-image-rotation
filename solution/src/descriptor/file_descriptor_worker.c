//
// Created by Илья Поветин on 20.12.2022.
//

#include "descriptor/file_descriptor_worker.h"

enum descriptor_operation_status open_file_for_read_and_get_status(FILE **file, const char *file_name) {

    if (file_name == NULL) {
        return DESCRIPTOR_OPERATION_FILENAME_IS_NULL;
    }

    *file = fopen(file_name, "rb");

    if (*file == NULL) {
        return DESCRIPTOR_OPERATION_FILE_NOT_FOUND;
    }

    return DESCRIPTOR_OPERATION_SUCCESS;

}

enum descriptor_operation_status open_file_for_write_and_get_status(FILE **file, const char *file_name) {

    if (file_name == NULL) {
        return DESCRIPTOR_OPERATION_FILENAME_IS_NULL;
    }

    *file = fopen(file_name, "wb");

    if (*file == NULL) {
        return DESCRIPTOR_OPERATION_FILE_NOT_FOUND;
    }

    return DESCRIPTOR_OPERATION_SUCCESS;

}

enum descriptor_operation_status close_file_and_get_status(FILE **file) {

    if (file == NULL) {
        return DESCRIPTOR_OPERATION_FILE_IS_NULL;
    }

    int closeStatus = fclose(*file);

    if (closeStatus) {
        return DESCRIPTOR_OPERATION_FILE_NOT_FOUND;
    }

    return DESCRIPTOR_OPERATION_SUCCESS;

}
