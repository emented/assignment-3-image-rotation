//
// Created by Илья Поветин on 20.12.2022.
//

#include "rotator/image_rotator.h"

#include "stdlib.h"

struct image rotate_image(struct image const image) {

    struct image rotated;

    rotated.height = image.width;
    rotated.width = image.height;

    rotated.data = malloc(sizeof(struct pixel) * rotated.width * rotated.height);

    if (rotated.data != NULL) {

        for (uint64_t i = 0; i < rotated.width; i++) {
            for (uint64_t j = 0; j < rotated.height; j++) {
                rotated.data[rotated.width + j * image.height - i - 1] = image.data[j + image.width * i];
            }
        }

    }
    return rotated;


}



